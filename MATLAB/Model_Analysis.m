%% Model Analysis

%% Flywheel Moment of Inertia

Al_den = 2700; %density of aluminium in kg/m^3
r_s = 0.04; %flywheel radius metres
w_s = 0.01;

V_sphere = (4/3)*pi*r_s^3

M_sphere = V_sphere*Al_den

V_disc = w_s*pi*r_s^2
M_disc = V_disc*Al_den

J_spin = 2*(2/5)*M_disc*r_s^2

%% Load Moment of Inertia due to flywheels

r_out = 0.1; %Distance between flywheel spin axes

J_load_fly = 2*M_disc*r_out^2

%% Load Weights

J_load = 20*J_spin - J_load_fly

% J_load = 2*M*r^2 %two point masses at r

r_load = 0.2

M = J_load/(2*r_load^2)




