function out = GyroActDynamics(in)

u = in(1);
t = 0;
x = in(2:5);

param = getParam;

out = gyroAct_contfun(t,u,x,param);