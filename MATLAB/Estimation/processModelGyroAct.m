function [xnext,A,SQ] = processModelGyroAct(x,u,param)

T = param.T;
J_l = param.J_l;                   % Moment of inertia about y axis [kg*m^2]
J_g = param.J_g;                    % Moment of inertia about precession axis [kg*m^2]
J_s = param.J_s;                   % Moment of inertia about spin axis [kg*m^2]

b_theta  = param.b_theta;   % viscous friction coeff on load about y-axis [N*m/s]
mu_theta = param.mu_theta;  % Coloumb friction coeff on load about y-axis [N*m]
b_alpha  = param.b_alpha;   % viscous friction coeff on gimbal about x-axis [N*m/s]
mu_alpha = param.mu_alpha;  % Coloumb friction coeff on gimbal about x-axis [N*m]

omega_s = param.omega_s;               % spin rate [rad/sec]

atan_lim = 1.570796326794897;

% This is the Euler discretisation
dx = gyroAct_fun([],u,x,param);
xnext = x + T*dx;

alpha_dot     = x(1);
theta_dot     = x(2);
alpha       = x(3);
theta       = x(4);
%J_l         = x(5);

if ~isempty(u)
    taup = u(1);
else
    taup = 0;
end

% This is the linearisation about x

A =  [b_theta, J_s*omega_s/J_g; J_s*omega_s/J_l, b_alpha; 1/J_l, 0; 0, 1/J_g];
A = [A, zeros(4,2)];


% Square-root of process noise covariance
SQ = diag([3e-3,3e-3,3e-3,3e-3]);