function [y,C,SR] = measurementModelGyroAct(x,u,param)

y = [x(1);x(2)];
C = blkdiag(eye(2), 0, 0); % Already linear

% Measurement noise covariance
SR = param.SR;       

