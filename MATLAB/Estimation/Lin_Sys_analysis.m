
J_l   = 6.2;   % Moment of inertia about y axis [kg*m^2]
J_g    = 0.04;  % Moment of inertia about precession axis [kg*m^2]
J_s    = 0.062; % Moment of inertia about spin axis [kg*m^2]

b_theta   = 1.5;  % viscous friction coeff on load about y-axis [N*m/s]
mu_theta  = 1.5;  % Coloumb friction coeff on load about y-axis [N*m]
b_alpha   = 1;    % viscous friction coeff on gimbal about x-axis [N*m/s]
mu_alpha  = 0.8;  % Coloumb friction coeff on gimbal about x-axis [N*m]

omega_s = 100;          % spin rate [1/sec]
 
% Linearised gyro-actuator

%syms    b_alpha b_theta J_l J_g J_s omega_s  

A =  [b_theta, J_s*omega_s/J_g; J_s*omega_s/J_l, b_alpha; 1/J_l, 0; 0, 1/J_g];
A = [A, zeros(4,2)];
B = [0; 1; 0; 0];

C = [0 0 1 0];

D = [0];

sys = ss(A,B,C,D)

Cs = [1 0 1 0];

Q = diag([2 0 2 0])
R = [1]

K = lqr(A,B,Q,R)
