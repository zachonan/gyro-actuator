function dx = gyroAct_fun(t,u,x,param)

J_l = param.J_l;                   % Moment of inertia about y axis [kg*m^2]
J_g = param.J_g;                    % Moment of inertia about precession axis [kg*m^2]
J_s = param.J_s;                   % Moment of inertia about spin axis [kg*m^2]

b_theta  = param.b_theta;   % viscous friction coeff on load about y-axis [N*m/s]
mu_theta = param.mu_theta;  % Coloumb friction coeff on load about y-axis [N*m]
b_alpha  = param.b_alpha;   % viscous friction coeff on gimbal about x-axis [N*m/s]
mu_alpha = param.mu_alpha;  % Coloumb friction coeff on gimbal about x-axis [N*m]

omega_s = param.omega_s;               % spin rate [rad/sec]

atan_lim = 1.570796326794897;

if ~isempty(u)
    taup = u(1);
else
    taup = 0;
end

if t > 2
    if t < 8
        taup = param.taup;
    end
end

%% Alejandro's Model
% L_theta     = x(1);
% L_alpha     = x(2);
% theta       = x(3);
% alpha       = x(4);
% J_l         = x(5);
% 
% dx = [ ...  
%     J_s*omega_s*cos(alpha)*L_alpha/J_p - b_theta*L_theta/J_l - mu_theta*atan(10*L_theta)/atan_lim; ...     
%     -J_s*omega_s*cos(alpha)*L_theta/J_l - b_alpha*L_alpha/J_p - mu_alpha*atan(10*L_alpha)/atan_lim + taup; ...  
%     L_theta/J_l; ...
%     L_alpha/J_p; ...
%     0 ... constant Load Moment of Inertia
%     ];

%% Zach's Model
alpha_dot     = x(1);
theta_dot     = x(2);
alpha       = x(3);
theta       = x(4);

dx = [ ...
    (-(J_s)*omega_s*cos(alpha)*theta_dot - b_alpha*alpha_dot - mu_alpha*atan(10*alpha_dot*J_g)/atan_lim + taup)/J_l; ...    
    ((J_s)*omega_s*cos(alpha)*alpha_dot - b_theta*theta_dot - mu_theta*atan(10*theta_dot*J_l)/atan_lim)/2*J_g; ...                     	
    alpha_dot; ...
    theta_dot; ...
    ];
