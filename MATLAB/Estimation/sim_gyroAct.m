
% Model parameters
param = getParam;
%input
param.taup = 0.5;
u = 0;%param.taup;

% Simulation parameters
T = 0.01;
t = (0:T:10);
options = odeset('MaxStep',0.1);

tau = zeros(1,length(t));
for i = 1:length(t)
    if t(i) > 2
        if t(i) < 8
            tau(i) = param.taup;
        end
    end
end

% Set initial condition
x0 = [ ...
    0; ...              % Initial y momentum
    0; ...              % Initial precession momentum
    deg2rad(0); ...              % Initial y position
    deg2rad(0) ...              % Initial precession position
    %param.J_l ...       % load moment of inertia
    ];

% Run the simulation
[t,x] = ode45(@(t,x) gyroAct_fun(t,u,x,param),t,x0,options);
x = x.';

%x(5,t>param.pTime) = param.pDrag;

figure(1);clf

subplot(5,1,1)
plot(t,tau) %Not actual
title('Input Gimbal Torque')
xlabel('Time [s]')
ylabel('\tau_{gimb} [Nm]')
grid on

subplot(5,1,2)
plot(t,x(1,:))
title('Load Angular Momentum')
xlabel('Time [s]')
ylabel('L_{load} [kg m^2/s]')
grid on

subplot(5,1,3)
plot(t,x(2,:))
title('Gimbal Angular Momentum')
xlabel('Time [s]')
ylabel('L_{gimb} [kg m^2/s]')
grid on

subplot(5,1,4)
plot(t,rad2deg(x(3,:)))
title('Load Angle')
xlabel('Time [s]')
ylabel('\theta [deg]')
grid on

subplot(5,1,5)
plot(t,rad2deg(x(4,:)))
title('Gimbal Angle')
xlabel('Time [s]')
ylabel('\alpha [deg]')
grid on


% subplot(5,1,5)
% plot(t,x(5,:))
% xlabel('Time [s]')
% ylabel('Load Moment of Inertia [kg m^2]')
% grid on


%%

% Simulated measurements

SR = diag([3e-1,3e-1]);       % Square-root of measurement noise covariance
param.SR = SR;

yHist = [x(3,:);x(4,:)] + SR*randn(2,length(t));

% Addtional parameters for state estimation
param.T = T;

%%
% Initial state
mu0 = [ ...
    x0(1) + 1; ...              % Initial y momentum
    x0(2) - 1; ...              % Initial precession momentum
    x0(3) + deg2rad(5); ...              % Initial y position
    x0(4) - deg2rad(5); ...              % Initial precession position
    %x0(5) + (0.4*(rand-0.5))*x0(5) ...       % load moment of inertia
    ];      % Initial vertical velocity
muEKF = mu0;
muUKF = mu0;

% mu = x0;

% Initial estimation error covariance
S = diag([.1,.1,.1,.1]); % Upper Cholesky factor
assert(isequal(S,triu(S)));
SEKF = S;
SUKF = S; % Upper Cholesky decomposition

muHistEKF = zeros(length(muEKF),length(t));
SdiagHistEKF = zeros(length(muUKF),length(t));

muHistUKF = zeros(length(muEKF),length(t));
SdiagHistUKF = zeros(length(muUKF),length(t));

for i = 1:length(t)
    u = param.taup;
    y = yHist(:,i);
    
    %[muEKF,SEKF] = EKF(muEKF,SEKF,u,y,@processModelGyroAct,@measurementModelGyroAct,param);
    %muHistEKF(:,i) = muEKF;
    %SdiagHistEKF(:,i) = realsqrt(diag(SEKF.'*SEKF)); % Square root of diagonal of P
    
    [muUKF,SUKF] = UKF(muUKF,SUKF,u,y,@processModelGyroAct,@measurementModelGyroAct,param);
    muHistUKF(:,i) = muUKF;
    SdiagHistUKF(:,i) = realsqrt(diag(SUKF.'*SUKF)); % Square root of diagonal of P
end

%% Plot figures
figure(2);clf
subplot(5,1,1)
plot(t,x(1,:),t,muHistEKF(1,:),t,muHistUKF(1,:))
xlabel('Time [s]')
ylabel('y ang velocity [rad/s]')
legend('True','EKF','UKF')
grid on
subplot(5,1,2)
plot(t,x(2,:),t,muHistEKF(2,:),t,muHistUKF(2,:))
xlabel('Time [s]')
ylabel('Precession Ang Velocity [rad/s]')
legend('True','EKF','UKF')
grid on
subplot(5,1,3)
plot(t,rad2deg(x(3,:)),t,rad2deg(muHistEKF(3,:)),t,rad2deg(muHistUKF(3,:)))
xlabel('Time [s]')
ylabel('y position [deg]')
legend('True','EKF','UKF')
grid on
subplot(5,1,4)
plot(t,rad2deg(x(4,:)),t,rad2deg(muHistEKF(4,:)),t,rad2deg(muHistUKF(4,:)))
xlabel('Time [s]')
ylabel('Precession position [deg]')
legend('True','EKF','UKF')
grid on
% subplot(5,1,5)
% plot(t,x(5,:),t,muHistEKF(5,:),t,muHistUKF(5,:))
% xlabel('Time [s]')
% ylabel('Load Moment of Inertia [kg m^2]')
% legend('True','EKF','UKF')
% grid on

%%
figure(4);clf
subplot(5,1,1)
semilogy(t,abs(x(1,:).'-muHistEKF(1,:)),t,abs(x(1,:).'-muHistUKF(1,:)))
title('Error')
xlabel('Time [s]')
ylabel('y ang velocity [rad/s]')
legend('EKF','UKF')
grid on
subplot(5,1,2)
semilogy(t,abs(x(2,:).'-muHistEKF(2,:)),t,abs(x(2,:).'-muHistUKF(2,:)))
xlabel('Time [s]')
ylabel('Precession Ang Velocity [rad/s]')
legend('EKF','UKF')
grid on
subplot(5,1,3)
semilogy(t,abs(x(3,:).'-muHistEKF(3,:)),t,abs(x(3,:).'-muHistUKF(3,:)))
xlabel('Time [s]')
ylabel('y position [deg]')
legend('EKF','UKF')
grid on
subplot(5,1,4)
semilogy(t,abs(x(4,:).'-muHistEKF(4,:)),t,abs(x(4,:).'-muHistUKF(4,:)))
xlabel('Time [s]')
ylabel('y position [deg]')
legend('EKF','UKF')
grid on
% subplot(5,1,5)
% semilogy(t,abs(x(5,:).'-muHistEKF(5,:)),t,abs(x(5,:).'-muHistUKF(5,:)))
% xlabel('Time [s]')
% ylabel('y position [deg]')
% legend('EKF','UKF')
% grid on

% figure(5);clf
% semilogy(t,SdiagHistEKF)
% title('$\sqrt{P_{ii}}$ for EKF','interpreter','latex')
% xlabel('Time [s]')
% legend({'$\sqrt{P_{11}}$','$\sqrt{P_{22}}$','$\sqrt{P_{33}}$'},'interpreter','latex')
% grid on
figure(6);clf
semilogy(t,SdiagHistUKF)
title('$\sqrt{P_{ii}}$ for UKF','interpreter','latex')
xlabel('Time [s]')
legend({'$\sqrt{P_{11}}$','$\sqrt{P_{22}}$','$\sqrt{P_{33}}$'},'interpreter','latex')
grid on