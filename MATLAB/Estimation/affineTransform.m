function [muy,Syy] = affineTransform(mux,Sxx,h)

%
% Square Root Affine Transform of y = h(x) + v
% where v ~ N(0,R) = N(0,SR.'*SR)
% given Jacobian C = dh/dx at x = mux
%

[muy,C,SR] = h(mux);
ny = length(muy);

R = triu(qr([Sxx*C.';SR],0));
Syy = R(1:ny,:);

Syy = sign(diag(Syy)).*Syy; % Choose factor with positive diagonal (QR is not unique)