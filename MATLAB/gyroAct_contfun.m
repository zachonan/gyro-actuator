function dx = gyroAct_contfun(t,u,x,param)

J_l = param.J_l;                   % Moment of inertia about y axis [kg*m^2]
J_g = param.J_g;                    % Moment of inertia about precession axis [kg*m^2]
J_s = param.J_s;                   % Moment of inertia about spin axis [kg*m^2]

b_theta  = param.b_theta;   % viscous friction coeff on load about y-axis [N*m/s]
mu_theta = param.mu_theta;  % Coloumb friction coeff on load about y-axis [N*m]
b_alpha  = param.b_alpha;   % viscous friction coeff on gimbal about x-axis [N*m/s]
mu_alpha = param.mu_alpha;  % Coloumb friction coeff on gimbal about x-axis [N*m]

omega_s = param.omega_s;               % spin rate [rad/sec]

atan_lim = 1.570796326794897;

taup = u(1);

if t > 2
    if t < 8
        taup = param.taup;
    end
end

% % %% Alejandro's Model
% L_alpha     = x(1);
% L_theta     = x(2);
% alpha       = x(3);
% theta       = x(4);
% 
% dx = [ ...
%     (-J_s*omega_s*cos(alpha)*L_theta/J_l - b_alpha*L_alpha^2/J_p^2 - mu_alpha*atan(10*L_alpha)/atan_lim + taup); ...    
%     (J_s*omega_s*cos(alpha)*L_alpha/J_p - b_theta*L_theta^2/J_l^2 - mu_theta*atan(10*L_theta)/atan_lim); ...                     	
%     L_alpha/J_p; ...
%     L_theta/J_l; ...
%     ];

%% Zach's Model
alpha_dot     = x(1);
theta_dot     = x(2);
alpha       = x(3);
theta       = x(4);

dx = [ ...
    (-(J_s)*omega_s*cos(alpha)*theta_dot - b_alpha*alpha_dot - mu_alpha*atan(10*alpha_dot*J_p)/atan_lim + taup)/J_l; ...    
    ((J_s)*omega_s*cos(alpha)*alpha_dot - b_theta*theta_dot - mu_theta*atan(10*theta_dot*J_l)/atan_lim)/J_p; ...                     	
    alpha_dot; ...
    theta_dot; ...
    ];

% % Flow Source Model
% L_alpha     = x(1);
% L_theta     = x(2);
% alpha       = x(3);
% theta       = x(4);
% 
% dx = [ ...
%     (-J_s*omega_s*cos(alpha)*L_theta/J_l - b_alpha*L_alpha/J_p - mu_alpha*atan(10*L_alpha)/atan_lim + taup); ...    
%     (J_s*omega_s*cos(alpha)*L_alpha/J_p - b_theta*L_theta/J_l - mu_theta*atan(10*L_theta)/atan_lim); ...                     	
%     L_alpha/J_p; ...
%     L_theta/J_l; ...
%     ];