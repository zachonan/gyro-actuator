%%
clear; clc;

param = getParam;

J_l = param.J_l;                   % Moment of inertia about y axis [kg*m^2]
J_p = param.J_p;                    % Moment of inertia about precession axis [kg*m^2]
J_s = param.J_s;                   % Moment of inertia about spin axis [kg*m^2]

b_theta  = param.b_theta;   % viscous friction coeff on load about y-axis [N*m/s]
mu_theta = param.mu_theta;  % Coloumb friction coeff on load about y-axis [N*m]
b_alpha  = param.b_alpha;   % viscous friction coeff on gimbal about x-axis [N*m/s]
mu_alpha = param.mu_alpha;  % Coloumb friction coeff on gimbal about x-axis [N*m]

omega_s = param.omega_s;               % spin rate [rad/sec]

tRun = 20;      % [sec]

stepON = 2;     % [sec]
stepOFF = 999;   % [sec]
alpha_dot_in = deg2rad(0);      % [rad/s]



x0 = [ ...
        0; ...              theta_dot [rad/sec]
        0; ...              alpha_dot [rad/sec]
        deg2rad(0); ...     theta [rad]
        deg2rad(0)  ...     alpha [rad]
        ];
 
ref = deg2rad(180); % reference



%% PID Controller
P = 200;
I = 0.01;
D = 10;
N = 1;

%% LQR Controller

% Linearised Model
% 
% A1 = [-b_alpha/J_p -J_s*omega_s/J_l; J_s*omega_s/J_p -b_theta/J_l];
% A2 = zeros(2);
% A3 = [1 0; 0 1];
% A = [A1 A2; A3 A2];
% 
% B1 = [1; 0];
% B = [B1; 0; 0];
% 
% C1 = [0 1]; %ouput theta_dot
% 
% C = [0 0 0 1]; %output theta
% 
% % Check controllability
% CO = ctrb(A, B);
% assert(rank(CO) == min(size(CO)), 'System not controllable!')
% 
% Q = diag([1e0 1e6]);
% R = 3.6e3;
% 
% K = lqr(A1,B1,Q,R);
% 
% N = inv(C1*(inv(-A1+B1*K))*B1);


%%

out = sim('GyroActuator_Stepper');

%%
% figure(1);clf
% 
% subplot(6,1,3)
% plot(out.t,out.theta_dot/J_l)
% title('Load Angular Momentum')
% xlabel('Time [s]')
% ylabel('L_{load} [kg m^2/s]')
% grid on
% 
% subplot(6,1,4)
% plot(out.t,out.alpha_dot/J_l)
% title('Gimbal Angular Momentum')
% xlabel('Time [s]')
% ylabel('L_{gimb} [kg m^2/s]')
% grid on
% 
% subplot(6,1,5)
% plot(out.t,out.theta)
% title('Load Angle')
% xlabel('Time [s]')
% ylabel('\theta [deg]')
% grid on
% 
% subplot(6,1,6)
% plot(out.t,out.alpha)
% title('Gimbal Angle')
% xlabel('Time [s]')
% ylabel('\alpha [deg]')
% grid on
