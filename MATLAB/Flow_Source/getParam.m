function [param] = getParam
% Returns GyroActuator Constant Parameters

param.J_l   = 6.2;   % Moment of inertia about y axis [kg*m^2]
param.J_p    = 0.04;  % Moment of inertia about precession axis [kg*m^2]
param.J_s    = 0.062; % Moment of inertia about spin axis [kg*m^2]

param.b_theta   = 1.5;  % viscous friction coeff on load about y-axis [N*m/s]
param.mu_theta  = 1.5;  % Coloumb friction coeff on load about y-axis [N*m]
param.b_alpha   = 1;    % viscous friction coeff on gimbal about x-axis [N*m/s]
param.mu_alpha  = 0.8;  % Coloumb friction coeff on gimbal about x-axis [N*m]

param.omega_s = 100;          % spin rate [1/sec]
 
end

