function [param] = getParam
% Returns GyroActuator Constant Parameters

param.J_l   = 0.030699442;   % Moment of inertia about y axis [kg*m^2]
param.J_p    = 0.000480929;  % Moment of inertia about precession axis [kg*m^2]
param.J_s    = 2*0.000421308; % Moment of inertia about spin axis [kg*m^2]

param.b_theta   = 0.01;  % viscous friction coeff on load about y-axis [N*m/s]
param.mu_theta  = 0;  % Coloumb friction coeff on load about y-axis [N*m]
param.b_alpha   = 0.8;    % viscous friction coeff on gimbal about x-axis [N*m/s]
param.mu_alpha  = 0;  % Coloumb friction coeff on gimbal about x-axis [N*m]

param.omega_s = 200;          % spin rate [1/sec]

end

