/* Spin Controller - an application for the Pololu Baby Orangutan B
 *
 * This application uses the Pololu AVR C/C++ Library.  For help, see:
 * -User's guide: http://www.pololu.com/docs/0J20
 * -Command reference: http://www.pololu.com/docs/0J18
 *
 * Created: 10/27/2020 11:33:28 AM
 *  Author: Zach
 */

#include <pololu/orangutan.h>
#include i2cmaster.h

int main()
{
	//Initialisation
	
		//Comms - I2C
	
		//Motors (automatically)

		//Encoders
		
		//Controller
	
	while(1)
	{
		//Read Comms
		
		//Read Encoder
		
		//Calculate Spin Velocity
		
		//Calculate Control Action
		
		//Set Motors
	}
}
