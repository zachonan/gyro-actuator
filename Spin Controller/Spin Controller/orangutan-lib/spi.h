// spi.h
//
// SPI master routines were pulled from the Atmel ATMega168 datasheet.

#ifndef _SPI_
#define _SPI_

// Loop until any current SPI transmissions have completed
#define spi_wait()	while (!(SPI_SPSR & (1 << SPI_SPIF)));

// Initialize the SPI subsystem
void spi_init();

// Transfer a byte of data
uint8_t spi_transfer( uint8_t data );

#endif // _SPI_
